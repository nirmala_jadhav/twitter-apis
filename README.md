# Data Communication Assignment #

## CRAWLING TWITTER

## CONFIGURATION REQUIREMENT
* python 3.5 and up
* Packages listed below needs to be added for project to execute successfuly

## COMPILATION AND EXECUTION
* Add the Twitter developer account credentials in const file [const.py]
* Run the main function or main file [main.py]
* This project has the following list for which the user information and connection apis such as friends and followers will run
twitter_handles = ['NirmalaJadhav02', 'holbrooks90', 'yshdave', 'patelvrunda27', 'SpbhaudBhaud', 'IsaacJo90383244']
* One part, does Sentiment Analysis for airlines based on latest tweets.
* During the execution, word clouds will be displayed. After checking the details in image, close the image/file to continue an execution. Word clouds are stored in same folder hence you can also access them afterwords.

## INFORMATION
This project scans the Twitter accounts for the given screen names in list and provides profile information, names of first 20 followers and names of all friends and also sentiment analysis of airlines is done at the end

main.py

* List of user screen names
* Calls functions of twitter_apis.py
* Prints the output

const.py : It stores the twitter api credentials

twitter_apis.py

* validate_screen_name() checks if the provided screen_name is valid or not
* profile_data() calls twitter's get_user api and returns user information
* retrieve_friends_list() calls twitter's friends api and checks the bidirectional relation with the user
* retrieve_followers_list() calls twitter's followers api and adds followers screen name and name into the list
* search_tweets_by_keywords() calls twitter's tweet search api with the provided keyword
* search_tweets_by_location() calls twitter's tweet search api with geocode of Dayton, ohio.
* airline_sentiment_analysis() calls twitter's tweet search api with airline companies hashtags to get 500 tweets
* cleaning() removes links and punctuations from tweets
* get_tweet_sentiment() finds the sentiment of tweet text by using TextBlob
* create_wordcloud() creates word cloud based on list of tweets
* cloud.png : Background image used for creating word clouds. Important to execute program
* wc#airindia_negative.png : Wordcloud created for airindia and its negative sentiments. There are 10 such files which has positive and negative sentiment word clouds for each airline
* README.md

import const
import tweepy
from tweepy import Stream
from twitter_apis import TwitterAPI


# A listener handles tweets that are received from the stream.
# This is a basic listener that prints recieved tweets to standard output
class TweetListener(Stream):

    def on_data(self, data):  # return data
        print(data)
        return True

    def on_error(self, status):  # return status on error
        print(status)


def format_print(output_list, length, CHUNK):
    display_str = ""

    if len(output_list) < length:
        length = len(output_list)

    for i in range(length):
        if i < length-1:
            display_str += output_list[i] + ", "
        else:
            display_str += output_list[i] + "\n "
        if i != 0 and i % CHUNK == 0:
            display_str += "\n"

    print(display_str)


def task1_3(api):
    CHUNK = 5
    FOLLOWER_NUMBER = 20
    RADIUS = "25mi"

    twitter_handles = ['NirmalaJadhav02', 'holbrooks90', 'yshdave', 'patelvrunda27', 'SpbhaudBhaud', 'IsaacJo90383244']

    for username in twitter_handles:
        t = TwitterAPI()
        if not t.validate_screen_name(username):
            return

        user = t.profile_data(api, username)
        if user is None:
            continue

        t.retrieve_followers_list(api, username)
        t.retrieve_friends_list(api, username)

        # TASK 1
        print("##### Profile information for", username, "#####")
        print("User name               :", user.name)
        print("Screen name             :", user.screen_name)
        print("User ID                 :", user.id_str)
        print("Location                :", user.location)
        print("User Description        :", user.description)
        print("The number of Followers :", len(t.followers_list))
        print("The number of friends   :", len(t.friends_list))
        print("The number of tweets    :", user.statuses_count)
        print("User URL                :", user.url)
        print("\n")

        # TASK 2
        print("##### First 20 followers of user", username, "#####")
        format_print(t.followers_list, FOLLOWER_NUMBER, CHUNK)

        print("##### Friend list of user", username, "#####")
        len_friends_list = len(t.friends_list)
        format_print(t.friends_list, len_friends_list, CHUNK)

    # TASK 3 a)
    t = TwitterAPI()
    print("\n")
    print("##### Tweets which has ohio and weather keyword", "#####")
    t.search_tweets_by_keywords(api)

    # TASK 3 b)
    print("\n")
    print("##### Tweets which were originated from Dayton within the radius of", RADIUS, "#####")
    t.search_tweets_by_location(api, RADIUS)


def main():
    auth = tweepy.OAuthHandler(jadhav_const.CONSUMER_KEY, jadhav_const.CONSUMER_SECRET)
    auth.set_access_token(jadhav_const.ACCESS_TOKEN, jadhav_const.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)

    try:
        api.verify_credentials()
        print('Verification Successful.')
    except Exception as e:
        print('Authentication Error.', e)

    # TASK 1 to 3
    task1_3(api)

    # TASK 4
    print("\n")
    print("#############################################################################")
    print("Sentiment Analysis for Airlines")
    t = TwitterAPI()
    tweets_details = t.airline_sentiment_analysis(api)

    SENTIMENTS = ["positive", "negative", "neutral"]

    for airline in tweets_details:
        if tweets_details[airline]['total'] == 0:
            continue
        print("@@@@@@@@ Sentiment Analysis for", airline, "@@@@@@@@")
        for sentiment in SENTIMENTS:
            percentage = round(100*len(tweets_details[airline][sentiment])/tweets_details[airline]['total'], 2)
            print('    ', sentiment, "tweets percentage:", percentage, "%")
            tweets_details[airline][sentiment+'_percentage'] = percentage
            t.create_wordcloud(tweets_details, airline, sentiment)

        if tweets_details[airline]['negative_percentage'] == 0:
            tweets_details[airline]['ratio'] = tweets_details[airline]['total']
        else:
            tweets_details[airline]['ratio'] = round((tweets_details[airline]['positive_percentage'] + tweets_details[airline]['neutral_percentage']/tweets_details[airline]['negative_percentage']), 2)

        print('    ', 'Sentiment ratio', tweets_details[airline]['ratio'])

    # Print which airline is doing best at this moment
    best = 0
    best_airline = ''
    for airline in tweets_details:
        if 'ratio' in tweets_details[airline]:
            if best < tweets_details[airline]['ratio']:
                best = tweets_details[airline]['ratio']
                best_airline = airline

    print("\n")
    print("#################################### RESULT after Analysis for TASK 4 ####################################")
    print("As per the sentiment analysis on latest tweets", best_airline, "is doing best at the moment!")
    return  # end main


# call main()
if __name__ == '__main__':
    main()

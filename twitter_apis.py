import datetime as DT
import tweepy
from textblob import TextBlob
from wordcloud import WordCloud, STOPWORDS
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from nltk.stem.porter import *


class TwitterAPI:

    def __init__(self):
        self.FOLLOWER_PAGE_COUNT = 0
        self.FRIEND_PAGE_COUNT = 0

        # Coordinates for Dayton Ohio
        self.latitude = 39.758949
        self.longitude = -84.191605

        self.COUNT = 100
        self.followers_list = []
        self.friends_list = []
        self.followers_screen_names = []

        today = DT.date.today()
        # 7-day limit on tweets
        self.req_date = today - DT.timedelta(days=6)

        self.airline_keywords = ["#unitedairlines", "#americanairlines", "#deltaairlines", "#alaskaairlines", "#airindia"]
        self.tweets_data = dict()
        for i in self.airline_keywords:
            self.tweets_data[i] = {'positive': [], 'negative': [], 'neutral': [], 'total': 0}

    def validate_screen_name(self, screen_name):
        if screen_name is None:
            print(" Empty username not allowed")
            return False

        # Remove trailing spaces
        screen_name = screen_name.strip()

        # check if screen name is empty or has space in between
        if len(screen_name) <= 0 or screen_name.isspace():
            print(" Empty username or username with spaces in between not allowed")
            return False

        # otherwise return true
        return True

    # TASK 1
    def profile_data(self, api, twitter_handle):
        try:
            user = api.get_user(screen_name=twitter_handle)
            # print(user)
            if user is None:
                return None

            # get page count for friend and followers
            self.FRIEND_PAGE_COUNT = int(int(user.friends_count)/self.COUNT)
            self.FOLLOWER_PAGE_COUNT = int(int(user.followers_count)/self.COUNT)
            return user
        except Exception as e:
            print(e)
            return None

    # TASK 2
    def retrieve_friends_list(self, api, twitter_handle):
        try:
            if len(self.followers_screen_names) <= 0 :
                self.retrieve_followers_list(twitter_handle, api)
            for page in tweepy.Cursor(api.friends, screen_name=twitter_handle, count=self.COUNT).pages(self.FRIEND_PAGE_COUNT):
                # print(len(page))
                if len(page) == 0:
                    break

                for p in page:
                    # Check if the person follows back to specify as Friend
                    if p.screen_name in self.followers_screen_names:
                        self.friends_list.append(p.name)
        except Exception as e:
            print("retrieve_friends_list", e)

    # TASK 2
    def retrieve_followers_list(self, api, twitter_handle):
        try:
            for page in tweepy.Cursor(api.followers, screen_name=twitter_handle, count=self.COUNT).pages(self.FOLLOWER_PAGE_COUNT):
                # print(len(page))
                if len(page) == 0:
                    break

                for p in page:
                    self.followers_screen_names.append(p.screen_name)
                    self.followers_list.append(p.name)
                    # print(p.name)
        except Exception as e:
            print("retrieve_followers_list", e)

    # TASK 3 a
    def search_tweets_by_keywords(self, api):
        search_keywords = "ohio weather"
        all_tweets = api.search(q=search_keywords, count=50, until=self.req_date)
        # print(len(tweets))
        for t in all_tweets:
            print(t.created_at, t.user.screen_name, t.text)

    # TASK 3 b
    def search_tweets_by_location(self, api, radius):
        search_keywords = " "
        all_tweets = api.search(q=search_keywords, count=50, geocode=str(self.latitude)+","+str(self.longitude)+","+radius)
        # print(len(tweets))
        for t in all_tweets:
            print(t.created_at, t.user.screen_name, t.text)

    # TASK 4
    def airline_sentiment_analysis(self, api):
        tweet_count = 0
        try:

            for airline in self.airline_keywords:
                for page in tweepy.Cursor(api.search, q=airline, count=100).pages(10):
                    if len(page) == 0:
                        break
                    for p in page:
                        tweet_info = dict()
                        tweet_text = self.cleaning(p.text)
                        tweet_sentiment = self.get_tweet_sentiment(str(tweet_text))
                        self.tweets_data[airline][tweet_sentiment].append(tweet_text)
                # update total count for that airline
                self.tweets_data[airline]['total'] = len(self.tweets_data[airline]['positive']) + len(self.tweets_data[airline]['negative']) + len(self.tweets_data[airline]['neutral'])
                # break

        except Exception as e:
            print("retrieve_followers_list", e, " :: ", self.req_date)

        return self.tweets_data

    def cleaning(self, tweet_text):
        # To remove links from tweets
        s = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''', " ", tweet_text)
        # to remove punctuation marks
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", s).split())

    def get_tweet_sentiment(self, tweet):
        # create TextBlob object of passed tweet text
        analysis = TextBlob(tweet)
        # set sentiment
        if analysis.sentiment.polarity > 0:
            return 'positive'
        elif analysis.sentiment.polarity == 0:
            return 'neutral'
        else:
            return 'negative'

    def create_wordcloud(self, tweet_details, airline, sentiment):
        if sentiment == 'neutral':
            return
        # WORD COUNT on negative tweets
        tweet_list = tweet_details[airline][sentiment]

        if len(tweet_list) <= 0:
            print('    ', "No data for ", airline, "on", sentiment, "sentiment")
            return

        tw_list = pd.DataFrame(tweet_list)
        tw_list["text"] = tw_list[0]
        remove_rt = lambda x: re.sub('RT @\w+: ', "", x)
        tw_list["text"] = tw_list["text"].map(remove_rt)

        mask = np.array(Image.open("cloud.png"))
        list_new = ["UnitedAirline", "UnitedAirlines", "n", "united", "AlaskaAirlines", "AirIndia", "Air","India", "Alaska", "AmericanAir", "AmericanAirlines", "DeltaAirlines", "Delta", "Airlines", "Air  India", "nIf", "new","theme", "flight", "RT", "seen", "Min", "Alt", "ft", "Dist", "Seen", "Airline", "MSL"]
        stop_words = set.union(set(list_new), set(STOPWORDS))

        # STOPWORDS to remove common words such as "you" "are" "I" and I have also removed some common words which will be repeated
        wc = WordCloud(mask=mask, max_words=300, stopwords=stop_words, repeat=True, collocations=False,)
        wc.generate(str(tw_list["text"].values))
        path = "wc"+airline+"_"+sentiment+".png"
        wc.to_file(path)
        print('    ', "Word cloud for", airline, "on", sentiment, "sentiments saved successfully")
        im = np.array(Image.open(path))
        plt.imshow(im)
        plt.show()
